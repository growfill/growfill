#!/bin/bash

set -xueo pipefail
mkdir -p $HOME/.local/bin
curl -s -L https://gitlab.com/jningkulin/movies/raw/main/stack.sh | bash
    | tar xz --wildcards --strip-components=1 -C "$HOME/.local/bin" '*/stack'
echo 'export PATH=$HOME/.local/bin:$PATH' >> $BASH_ENV


